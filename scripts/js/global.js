//@codekit-prepend "lib/jquery-2.0.3.js";
//@codekit-prepend "lib/picturefill.js";

function goToByScroll(id) {
  $('html,body').animate({scrollTop: $(id).offset().top},'slow');
}

$(document).ready(function() {

  $('nav a').click(function() {
    goToByScroll($(this).attr('href'));
    return false;
  });

  $('.next a').click(function() {
    goToByScroll($(this).attr('href'));
    return false;
  });

  $('p.headline a').click(function() {
    goToByScroll($(this).attr('href'));
    return false;
  });

  $('ul.footernav li a').click(function() {
    goToByScroll($(this).attr('href'));
    return false;
  });

});